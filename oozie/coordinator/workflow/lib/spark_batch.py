from pyspark.sql import SparkSession
from pyspark.sql.functions import to_date, count, lit, first
import sys

spark = SparkSession.builder\
    .appName("Spark Batch")\
    .enableHiveSupport()\
    .getOrCreate()

filepath = sys.argv[1]

spark.sql("create table if not exists log_counting (date_ date, hour char(2), cnt int)")

df = spark.read.format("parquet").option("basePath", "/user/nuriggum/log_data/").load(filepath)
df.select(first("date").alias("date_"), first("hour").alias("hour"), count("value").alias("cnt")) \
    .write.format("hive").mode("append").saveAsTable("log_counting")