from pyspark.sql import SparkSession
from pyspark.sql.functions import regexp_extract, to_timestamp, hour, to_date, format_string

spark = SparkSession.builder\
    .appName("Kafka Subscriber")\
    .getOrCreate()

df = spark.readStream.format("kafka")\
    .option("kafka.bootstrap.servers", "hdp-node2.nuriggum.kr:6667,hdp-node3.nuriggum.kr:6667")\
    .option("subscribe", "http")\
    .load()\

df.selectExpr("CAST(value as STRING) as value")\
    .withColumn("datetime", to_timestamp(regexp_extract("value", "\\[(.+) \\+\d+\\]", 1), "dd/MMM/yyyy:HH:mm:ss"))\
    .select(format_string("%02d", hour("datetime")).alias("hour"), to_date("datetime", "yyyy-MM-dd").alias("date"), "value")\
    .writeStream\
    .trigger(processingTime='10 minutes')\
    .format("parquet").outputMode("append")\
    .option("path", "/user/nuriggum/log_data")\
    .option("checkpointLocation", "/tmp/nuriggum/checkpoint")\
    .partitionBy("date", "hour")\
    .start()\
    .awaitTermination()
